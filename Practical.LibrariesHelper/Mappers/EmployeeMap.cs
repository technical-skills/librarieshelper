﻿using CsvHelper.Configuration;
using Practical.LibrariesHelper.Models;

namespace Practical.LibrariesHelper.Mappers
{
    public sealed class EmployeeMap : ClassMap<EmployeeModel>
    {
        public EmployeeMap()
        {
            Map(m => m.FirstName).Name(Constants.CsvHeaders.FirstName);
            Map(m => m.LastName).Name(Constants.CsvHeaders.LastName);
            Map(m => m.Address).Name(Constants.CsvHeaders.Address);
            Map(m => m.City).Name(Constants.CsvHeaders.City);
            Map(m => m.Mobile).Name(Constants.CsvHeaders.Mobile);
            Map(m => m.Email).Name(Constants.CsvHeaders.Email);
            Map(m => m.Salary).Name(Constants.CsvHeaders.Salary);
            Map(m => m.Direction).Name(Constants.CsvHeaders.Direction);
        }
    }
}
