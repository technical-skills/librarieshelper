﻿namespace Practical.LibrariesHelper
{
    public static class Constants
    {
        public class CsvHeaders
        {
            public const string FirstName = "FirstName";
            public const string LastName = "LastName";
            public const string Address = "Address";
            public const string City = "City";
            public const string Mobile = "Mobile";
            public const string Email = "Email";
            public const string Direction = "Direction";
            public const string Salary = "Salary";
        }
    }
}
