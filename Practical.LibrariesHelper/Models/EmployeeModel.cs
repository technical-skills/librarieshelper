﻿using CsvHelper.Configuration.Attributes;

namespace Practical.LibrariesHelper.Models
{
    public class EmployeeModel
    {
        [Name(Constants.CsvHeaders.FirstName)]
        public string FirstName { get; set; }

        [Name(Constants.CsvHeaders.LastName)]
        public string LastName { get; set; }

        [Name(Constants.CsvHeaders.Address)]
        public string Address { get; set; }

        [Name(Constants.CsvHeaders.City)]
        public string City { get; set; }

        [Name(Constants.CsvHeaders.Mobile)]
        public string Mobile { get; set; }

        [Name(Constants.CsvHeaders.Email)]
        public string Email { get; set; }

        [Name(Constants.CsvHeaders.Salary)]
        public string Salary { get; set; }

        [Name(Constants.CsvHeaders.Direction)]
        public string Direction { get; set; }
    }
}
