﻿namespace Practical.LibrariesHelper.Models
{
    public class WrapperInput
    {
        public List<EmployeeModel> EmployeeModels { get; set; }
        public IFormFile FileCSV { get; set; }
    }
}
