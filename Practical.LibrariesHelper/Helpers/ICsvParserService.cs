﻿using Practical.LibrariesHelper.Models;

namespace Practical.LibrariesHelper.Helpers
{
    public interface ICsvParserService
    {
        List<EmployeeModel> ReadCsvFileToEmployeeModel(IFormFile csvImport);
        string WriteNewCsvFile(string fileName, List<EmployeeModel> employeeModels);
    }
}
