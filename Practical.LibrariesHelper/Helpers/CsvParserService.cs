﻿using CsvHelper;
using Practical.LibrariesHelper.Mappers;
using Practical.LibrariesHelper.Models;
using System.Globalization;
using System.Text;

namespace Practical.LibrariesHelper.Helpers
{
    public class CsvParserService : ICsvParserService
    {
        /// <summary>
        ///  Handle Read CSV.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public List<EmployeeModel> ReadCsvFileToEmployeeModel(IFormFile fileCSV)
        {
            try
            {
                var fileExtension = Path.GetExtension(fileCSV.FileName);
                var fileName = Guid.NewGuid().ToString() + fileExtension;
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "files", fileName);
                using (FileStream fs = System.IO.File.Create(filePath))
                {
                    fileCSV.CopyTo(fs);
                }

                //Read File.
                using (var reader = new StreamReader(filePath, Encoding.Default))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    csv.Context.RegisterClassMap<EmployeeMap>();
                    var records = csv.GetRecords<EmployeeModel>().ToList();
                    return records;
                }
            }
            catch (UnauthorizedAccessException e)
            {
                throw new Exception(e.Message);
            }
            catch (FieldValidationException e)
            {
                throw new Exception(e.Message);
            }
            catch (CsvHelperException e)
            {
                throw new Exception(e.Message);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public string WriteNewCsvFile(string fileName, List<EmployeeModel> employeeModels)
        {
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "files", fileName + ".csv");

            using (StreamWriter sw = new StreamWriter(filePath, false, new UTF8Encoding(true)))
            using (CsvWriter cw = new CsvWriter(sw, CultureInfo.InvariantCulture))
            {
                cw.WriteHeader<EmployeeModel>();
                cw.NextRecord();
                foreach (EmployeeModel emp in employeeModels)
                {
                    cw.WriteRecord(emp);
                    cw.NextRecord();
                }
            }
            return filePath;
        }
    }
}

