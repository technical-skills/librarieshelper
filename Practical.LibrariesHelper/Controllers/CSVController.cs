﻿using Microsoft.AspNetCore.Mvc;
using Practical.LibrariesHelper.Helpers;
using Practical.LibrariesHelper.Models;

namespace Practical.LibrariesHelper.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CSVController : ControllerBase
    {
        private readonly ICsvParserService _csvParserService;

        public CSVController(ICsvParserService csvParserService)
        {
            _csvParserService = csvParserService;
        }

        [HttpPost("ReaderCSV")]
        public IEnumerable<EmployeeModel> ReaderEmployeeFromCSV(IFormFile csvImport)
        {
            Console.WriteLine("**** Read a CSV file *****");
            return _csvParserService.ReadCsvFileToEmployeeModel(csvImport);
        }

        [HttpPost("WriteCSV")]
        public ActionResult WriteEmployeeFromCSV(string fileName, IFormFile file)
        {
            Console.WriteLine("**** Read a CSV file *****");
            var result = _csvParserService.ReadCsvFileToEmployeeModel(file);
            var employee = new EmployeeModel()
            {
                Address = "351A Lạc Long Quân Phường 5 Quận 11",
                City = "Hồ Chí Minh",
                Direction = "Direction 20",
                FirstName = "Cuong",
                Email = "nqcuong720@mail.com",
                LastName = "Nguyen",
                Mobile = "0377077630",
                Salary = "100000"
            };
            result.Add(employee);
            Console.WriteLine("**** Write a CSV file *****");
            var filePath = _csvParserService.WriteNewCsvFile(fileName, result);
            var bytes = System.IO.File.ReadAllBytes(filePath);
            return File(bytes, "text/plain", Path.GetFileName(filePath));
        }
    }
}
